package com.example.julian.rooomdaohomework;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    EditText UserText;
    TextView UserList;
    ListView ClassList;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UserText = findViewById(R.id.USer);
        UserList = findViewById(R.id.Users);
        ClassList = findViewById(R.id.UserList);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.mylist, arrayList);
        ClassList.setAdapter(adapter);
        findViewById(R.id.Update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Update();

            }
        });
        findViewById(R.id.Nubutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Nuke();
            }
        });
        findViewById(R.id.Add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Add();
            }
        });
    }


    public void Add() {
        arrayList.add(UserText.getText().toString().trim());
        adapter.notifyDataSetChanged();
        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                Task task = new Task();
                task.setUser(UserText.getText().toString().trim());

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().taskDao().insert(task);
                return null;


            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                System.out.println("Saved");
            }


        }

        SaveTask st = new SaveTask();
        st.execute();


    }


    public void Update() {

        final AsyncTask<Void, Void,  List<Task>> task =
                new AsyncTask<Void, Void,  List<Task>>() {

                    @Override
                    protected  List<Task> doInBackground(Void... voids) {
                        List<Task> taskList = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().taskDao().getAll();
                        return taskList;
                    }

                    @Override
                    protected void onPostExecute(List<Task> response) {
                        getTasks(response);
                    }
                };
        task.execute();

    }


 public void Nuke() {
     UserList.setText("");
     arrayList.clear();
     adapter.notifyDataSetChanged();
     final AsyncTask<Void, Void,  Boolean> task =
             new AsyncTask<Void, Void, Boolean>() {

                 @Override
                 protected  Boolean doInBackground(Void... voids) {
                     DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().taskDao().nukeTable();
                     return true;
                 }

                 @Override
                 protected void onPostExecute(Boolean response) {
                     System.out.println("Nuked");
                 }
             };
     task.execute();

 }


    public void getTasks(List<Task> yes) {
        System.out.println(String.valueOf(yes.size()));
        String z = "";
        for (int i = 0; i < yes.size(); i++) {
                Task t = yes.get(i);
             z = z + " " + t.getUser();
             System.out.println(z);
             UserList.setText(z);
            }
            }

}
